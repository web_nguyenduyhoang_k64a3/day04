<?php
$nameErr = $falcutyErr = $genderErr = $dobErr = $addressErr = "";
$name = $falcuty = $gender = $dob = $address = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $nameErr = "Bạn cần điền họ tên";
    } else if (!preg_match("/^[a-zA-Z-' ]*$/", $name)) {
        $nameErr = "Tên chỉ chứa chữ cái và khoảng trắng";
    } else {
        $name = test_input($_POST["name"]);
    }

    if (empty($_POST["falcuty"])) {
        $emailErr = "Bạn cần điển tên khoa";
    } else {
        $email = test_input($_POST["falcuty"]);
    }


    if (empty($_POST["gender"])) {
        $genderErr = "Bạn cần chọn giới tính";
    } else {
        $gender = test_input($_POST["gender"]);
    }

    if (empty($_POST["dob"])) {
        $dobErr = "Bạn cần chọn ngày sinh";
    } else {
        $dob = test_input($_POST["dob"]);
    }

    $dob = DateTime::createFromFormat('dd/mm/yyyy', $_POST["dob"]);
    $date_errors = DateTime::getLastErrors();
    if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
        $dobErr = 'Bạn cần nhập đúng định dạng nagyf tháng';
    }
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
